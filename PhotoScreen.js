import React from 'react';
import { StyleSheet, Text, View, Button, Image, ImageBackground,TouchableOpacity,Dimensions  } from 'react-native';
import { ColorPicker } from 'react-native-color-picker'
import { Camera} from 'expo-camera';
import { Permissions } from 'expo-permissions';


export default class PhotoScreen extends React.Component {
      
  constructor(props) {
    super(props);
    
    this.state = {
       image:props.route.params.image
    };
  }


  async componentDidMount() {


    const form = new FormData();
    form.append("image", {
      uri: this.state.image,
      type: 'image/jpeg',
      name: "lalala"
    });
    



console.log(form)

    

    fetch('http://167.71.75.66:5002/processImage', {
      method: 'post',
      headers: {
        'Content-Type': 'multipart/form-data',
        'Accept': 'application/json',
        'Accept-Encoding':'gzip, deflate, br',
        'Connection':'keep-alive'
      },
      body: form
    })
    .then(response =>response.json()) // returns promise
    .then(responseJson => {
      console.log(responseJson)
    alert("ваша свинка синяя? " + responseJson)
    })
    .catch((error) => {
      console.log(error);
      alert("какая-то ошибка! У вас начался зомби апокалипсис?")
    })
   
  };


  render() {
      return (
        <View style={styles.container}>
   
      <Image style={styles.image} source={{uri:this.state.image}} />

      
        </View>
        );
    }
  }
  
  
  const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    title: {
      
      fontSize:27,
      color: '#000000'
    },
    image: {
      flex: 1,
            resizeMode: 'center'
    }

  });
  
  