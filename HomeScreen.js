import React from 'react';
import { StyleSheet, Text, View, Button, Image,ImageBackground, TouchableHighlight } from 'react-native';
import { ColorPicker } from 'react-native-color-picker'


export default class HomeScreen extends React.Component {
    render() {
      const {navigate} = this.props.navigation;
      return (



<View style={styles.container}>
<ImageBackground style={styles.imgBackground} source={require('./assets/123.jpg')} >


<Text style={styles.title}>Аттестационный проект
    по информатике на тему:
    «Палитра цветов системы цветопередачи»
    
    
    </Text>

    <TouchableHighlight onPress={() => navigate('Camera')}>
      <Image source={require('./assets/999.png')}   style={{width: 75, height: 60, resizeMode:'cover'}} accessibilityRole='imagebutton' /> 
  </TouchableHighlight>


        
   
   
          
    
    
    
  </ImageBackground>   
        </View>
  
   
        );
    }
  }



  
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#000000',
      
      
      alignItems: 'center',
      justifyContent: 'center',
    },
    title: {
      textAlign: 'center',
      fontSize:27,
      color: '#ffffff'
    },
    imgBackground: {
      width: '100%',
      height: '100%',
      flex: 1 ,
      alignItems: 'center',
      justifyContent: 'center'
}
  });
  