import React from 'react';
import { StyleSheet, Text, View, Button, Image, ImageBackground,TouchableOpacity,Dimensions  } from 'react-native';
import { ColorPicker } from 'react-native-color-picker'
import { Camera} from 'expo-camera';
import { Permissions } from 'expo-permissions';


export default class HomeScreen extends React.Component {
      
  camera = null;

  state = {
      hasCameraPermission: null,
      type : Camera.Constants.Type.back,
      image:null
      
  };

  async componentDidMount() {
    
    const camera = await Camera.requestPermissionsAsync();
    const hasCameraPermission = (camera.status === 'granted');
    this.setState({ hasCameraPermission:hasCameraPermission});

    //console.log("cam ratio" + this.camera.getSupportedRatiosAsync() );
  };

setupCamera = async () => {
  const ratios = await this.camera.getSupportedRatiosAsync();
        
  console.log("ratios " + ratios);
  // See if the current device has your desired ratio, otherwise get the maximum supported one
            // Usually the last element of "ratios" is the maximum supported ratio

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

            const wantedRatio = height/width
            var bestRatio = 0;
            var bestRatioError = 100000;
            for (var i in ratios) {
                const r = ratios[i].split(":")
                if (Math.abs(wantedRatio - r[0]/r[1]) < bestRatioError) {
                    bestRatioError = Math.abs(wantedRatio - r[0]/r[1])
                    bestRatio = ratios[i]
                }
            }            
console.log("ratio " + bestRatio);
            this.setState({
                ratio:bestRatio
            });
}

takePicture = () => {
  console.log("shot!!!");
  const {navigate} = this.props.navigation;

  this.camera.takePictureAsync({ skipProcessing: true }).then((data) => {
        this.setState({image:data.uri})
        
        console.log(data.uri)   
        navigate('Photo',{image:data.uri})

     })
  }

  render() {


    const { hasCameraPermission } = this.state;
    const { type } = this.state;
    const { image } = this.state;

    
    
    

    if (hasCameraPermission === null) {
        return (
          <View style={styles.container}>
            <Text style={styles.text}>Нет доступа к камере! Вы запускаете это приложение на унитазе?</Text>
          </View>
        )
    } else if (hasCameraPermission === false) {
      return (
        <View style={styles.container}>
          <Text style={styles.text}>вы отклонили доступ к камере</Text>
        </View>
      )
  }

      return (
        <View style={styles.container}>
   
         <Camera style={{ flex: 1 }} type={type}  ratio={this.state.ratio} 
         
         onCameraReady={this.setupCamera}
         ref={ref => {this.camera = ref}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'transparent',
            flexDirection: 'row',
          }}>
          
          <TouchableOpacity
            style={{
              flex: 0.1,
              alignSelf: 'flex-end',
              alignItems: 'center',
            }}
            onPress={() => {
              var itype = 
                type === Camera.Constants.Type.back
                  ? Camera.Constants.Type.front
                  : Camera.Constants.Type.back

                  console.log("ttt " + type + ","+ itype);
                  this.setState({type:itype})
              
            }}>
            <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> Flip </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={{
              flex: 0.9,
              alignSelf: 'flex-end',
              alignItems: 'center',
            }}
            onPress={this.takePicture}>
            <Text style={{ fontSize: 18, marginBottom: 10, color: 'white' }}> СТРЕЛЛЯЙ! </Text>
          </TouchableOpacity>

          
        </View>
        
      </Camera>
        </View>
        );
    }
  }
  
  
  const styles = StyleSheet.create({
    container: {
      flex: 1
    },
    title: {
      
      fontSize:27,
      color: '#000000'
    },
    text: {
      
      fontSize:27,
      color: '#ffffff'
    }

  });
  
  